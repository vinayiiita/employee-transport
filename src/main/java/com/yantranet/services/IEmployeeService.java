package com.yantranet.services;

import com.yantranet.entities.Employee;
import io.vertx.core.Future;

import java.util.List;
import java.util.Optional;

public interface IEmployeeService  {

    Future<Employee> create(Employee employee);

    Future<List<Employee>> getAll();

    Future<Optional<Employee>> getEmployee(String id);

    Future<Employee> update(Employee employee);

    Future<Boolean> delete(String id);
}
