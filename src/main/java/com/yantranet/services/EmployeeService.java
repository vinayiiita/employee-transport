package com.yantranet.services;

import com.yantranet.Constants;
import com.yantranet.entities.Employee;
import com.yantranet.jsonconverter.EmployeeConverter;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.redis.RedisClient;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class EmployeeService implements IEmployeeService {
    private final Vertx vertx;
    private final RedisClient redis;

    private final static Logger Logger = LoggerFactory.getLogger(EmployeeService.class);

    public EmployeeService(Vertx vertx, RedisClient redis) {
        this.vertx = vertx;
        this.redis = redis;
    }

    @Override
    public Future<Employee> create(Employee employee) {
        Future<Employee> result = Future.future();

        final String encoded = Json.encodePrettily(employee);
        redis.hset(Constants.EMPLOYEE_KEY, String.valueOf(employee.getId()),
                encoded, res -> {
                    if (res.succeeded()) {
                        redis.hget(Constants.EMPLOYEE_KEY, String.valueOf(employee.getId()),res2 -> {
                           if(res.succeeded()){
                               result.complete(new Employee(res2.result(),false));
                           }
                        });
                        //result.complete(true);
                    }           else
                        result.fail(res.cause());
                });
        return result;
    }

    @Override
    public Future<List<Employee>> getAll() {
        Future<List<Employee>> result = Future.future();
        redis.hvals(Constants.EMPLOYEE_KEY, res -> {
            if (res.succeeded()) {
                result.complete(res.result()
                        .stream()
                        .map(x -> new Employee((String) x,false
                        ))
                        .collect(Collectors.toList()));
            } else
                result.fail(res.cause());
        });
        return result;
    }

    @Override
    public Future<Optional<Employee>> getEmployee(String id) {
        Future<Optional<Employee>> result = Future.future();
        redis.hget(Constants.EMPLOYEE_KEY, id, res -> {
            if (res.succeeded()) {
                result.complete(Optional.ofNullable(
                        res.result() == null ? null : new Employee(res.result(),false)));
            } else
                result.fail(res.cause());
        });
        return result;

    }

    @Override
    public Future<Employee> update(Employee employee) {
        return this.getEmployee(employee.getId()).compose(old -> {
            if (old.isPresent()) {
                Employee emp = old.get().merge(employee);
                return this.create(emp);
                        //.map(r -> r!=null ? emp : null);
            } else {
                return Future.succeededFuture();
            }
        });

    }

    @Override
    public Future<Boolean> delete(String id) {
        Future<Boolean> result = Future.future();
        redis.hdel(Constants.EMPLOYEE_KEY, id, res -> {
            if (res.succeeded())
                result.complete(true);
            else
                result.complete(false);
        });
        return result;
    }


}
