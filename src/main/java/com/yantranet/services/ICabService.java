package com.yantranet.services;

import com.yantranet.entities.Cab;
import io.vertx.core.Future;

import java.util.List;
import java.util.Optional;

public interface ICabService  {


    Future<Boolean> create(Cab cab);

    Future<List<Cab>> getAll();

    Future<Optional<Cab>> getCab(String id);

    Future<Cab> update(Cab cab);

    Future<Boolean> delete(String id);

    Future<Cab> updateStatus(String id, Cab.CAB_STATUS status);
}
