package com.yantranet.services;

import com.yantranet.entities.Request;
import io.vertx.core.Future;

import java.util.Optional;

public interface IRequest {

    Future<String> create(Request request);

    Future<Optional<Request>> getRequest(String id);

}
