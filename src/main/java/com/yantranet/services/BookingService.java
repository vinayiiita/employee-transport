package com.yantranet.services;

import com.yantranet.entities.Booking;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.redis.RedisClient;

import java.util.Optional;

public class BookingService implements IBooking {

    private final Vertx vertx;
    private final RedisClient redis;

    private final static io.vertx.core.logging.Logger Logger = LoggerFactory.getLogger(CabService.class);

    public BookingService(Vertx vertx, RedisClient redis) {
        this.vertx = vertx;
        this.redis = redis;
    }


    @Override
    public Future<Optional<Booking>> getBooking(String id) {
        return null;
    }
}
