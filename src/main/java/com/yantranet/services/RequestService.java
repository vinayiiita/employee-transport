package com.yantranet.services;

import com.yantranet.Constants;
import com.yantranet.ErrorCode;
import com.yantranet.entities.Cab;
import com.yantranet.entities.Request;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.redis.RedisClient;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RequestService implements IRequest {

    private final Vertx vertx;
    private final RedisClient redis;

    private final static io.vertx.core.logging.Logger Logger = LoggerFactory.getLogger(RequestService.class);

    ICabService cabService;

    public RequestService(Vertx vertx, RedisClient redis) {
        this.vertx = vertx;
        this.redis = redis;
        cabService = new CabService(vertx, redis);
    }

    @Override
    public Future<String> create(Request request) {
        Future<String> result = Future.future();
        redis.hvals(Constants.CAB_KEY, res -> {
            if (res.succeeded()) {
                List<Cab> cabList = res.result()
                        .stream()
                        .map(x -> new Cab((String) x, false
                        ))
                        .collect(Collectors.toList());
                boolean isAvailable = false;
                for(Cab cab:cabList){
                    if(cab.getStatus().equals(Cab.CAB_STATUS.AVAILABLE)){
                        isAvailable = true;
                        break;
                    }
                }
                Logger.info(isAvailable);
                if(isAvailable){
                    final String encoded = Json.encodePrettily(request);
                    redis.hset(Constants.REQUEST_KEY,String.valueOf(request.getId()),encoded,res2 -> {
                        if(res2.succeeded()){
                            //result.complete(request.getId());
                            //redis.hexists(Constants.SOURCE,res2 ->)
                            redis.hvals(Constants.SOURCE,res3 -> {
                                if(res3.succeeded()){
                                    //res3.result().st


                                }
                            });
                        }
                    });
                }
                else{
                    result.complete(ErrorCode.CAB_NOT_AVAILABLE);
                }
            }
        });

       // Logger.info(result.result());
        return result;
    }
/*
cabs.setHandler( cabList -> {
            if(cabList.succeeded()){
                for(Cab cab:cabList.result()){
                    Logger.info(cab.getId());
                }
            }
        });
 */
    @Override
    public Future<Optional<Request>> getRequest(String id) {
        Future<Optional<Request>> result = Future.future();
        redis.hget(Constants.REQUEST_KEY, id, res -> {
            if (res.succeeded()) {
                result.complete(Optional.ofNullable(
                        res.result() == null ? null : new Request(res.result(), false)));
            } else
                result.fail(res.cause());
        });
        return result;
    }
}
