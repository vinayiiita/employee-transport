package com.yantranet.services;

import com.yantranet.Constants;
import com.yantranet.entities.Cab;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.redis.RedisClient;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CabService implements ICabService {
    private final Vertx vertx;
    private final RedisClient redis;

    private final static io.vertx.core.logging.Logger Logger = LoggerFactory.getLogger(CabService.class);

    public CabService(Vertx vertx, RedisClient redis) {
        this.vertx = vertx;
        this.redis = redis;
    }
    @Override
    public Future<Boolean> create(Cab cab) {
        Future<Boolean> result = Future.future();
        final String encoded = Json.encodePrettily(cab);
        redis.hset(Constants.CAB_KEY, String.valueOf(cab.getId()),
                encoded, res -> {
                    if (res.succeeded())
                        result.complete(true);
                    else
                        result.fail(res.cause());
                });
        return result;
    }

    @Override
    public Future<List<Cab>> getAll() {
        Future<List<Cab>> result = Future.future();
        redis.hvals(Constants.CAB_KEY, res -> {
            if (res.succeeded()) {
                result.complete(res.result()
                        .stream()
                        .map(x -> new Cab((String) x,false
                        ))
                        .collect(Collectors.toList()));
            } else
                result.fail(res.cause());
        });
        return result;
    }

    @Override
    public Future<Optional<Cab>> getCab(String id) {
        Future<Optional<Cab>> result = Future.future();
        redis.hget(Constants.CAB_KEY, id, res -> {
            if (res.succeeded()) {
                result.complete(Optional.ofNullable(
                        res.result() == null ? null : new Cab(res.result(),false)));
            } else
                result.fail(res.cause());
        });
        return result;
    }

    @Override
    public Future<Cab> update(Cab cab) {
        return this.getCab(cab.getId()).compose(old -> {
            if (old.isPresent()) {
                Cab fnTodo = old.get().merge(cab);
                return this.create(fnTodo)
                        .map(r -> r ? fnTodo : null);
            } else {
                return Future.succeededFuture();
            }
        });
    }

    @Override
    public Future<Boolean> delete(String id) {
        Future<Boolean> result = Future.future();
        redis.hdel(Constants.CAB_KEY, id, res -> {
            if (res.succeeded())
                result.complete(true);
            else
                result.complete(false);
        });
        return result;
    }

    @Override
    public Future<Cab> updateStatus(String id,Cab.CAB_STATUS status) {
        Cab cab = new Cab();
        cab.setId(id);
        cab.setStatus(status);
        //cab.setRegistrationNumber("haha");
        return this.getCab(id).compose(old -> {
            if (old.isPresent()) {
                Cab fnTodo = old.get().merge(cab);
                return this.create(fnTodo)
                        .map(r -> r ? fnTodo : null);
            } else {
                return Future.succeededFuture();
            }
        });
    }
}
