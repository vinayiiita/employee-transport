/*
package com.yantranet.services;

import com.yantranet.Constants;
import com.yantranet.entities.Cab;
import com.yantranet.entities.Source;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.redis.RedisClient;

public class SourceService {
    private final Vertx vertx;
    private final RedisClient redis;

    private final static io.vertx.core.logging.Logger Logger = LoggerFactory.getLogger(SourceService.class);

    public SourceService(Vertx vertx, RedisClient redis) {
        this.vertx = vertx;
        this.redis = redis;
    }
    public void create(Source source) {
        Future<Boolean> result = Future.future();
        final String encoded = Json.encodePrettily(source);
        redis.hset(Constants.CAB_KEY, String.valueOf(source.getId()),
                encoded, res -> {
                    if (res.succeeded())
                        result.complete(true);
                    else
                        result.fail(res.cause());
                });
        //return result;
    }
}
*/
