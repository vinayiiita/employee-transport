package com.yantranet.services;

import com.yantranet.entities.Booking;
import io.vertx.core.Future;

import java.util.Optional;

public interface IBooking {

    Future<Optional<Booking>> getBooking(String id);

}
