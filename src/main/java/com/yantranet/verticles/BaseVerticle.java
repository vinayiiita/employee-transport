package com.yantranet.verticles;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.redis.RedisClient;
import io.vertx.redis.RedisOptions;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;

public class BaseVerticle extends AbstractVerticle{

    private static final Logger LOGGER = LoggerFactory.getLogger(BaseVerticle.class);

    private static final String HTTP_HOST = "0.0.0.0";
    private static final int HTTP_PORT = 8080;

    private static final String REDIS_HOST = "127.0.0.1";
    private static final int REDIS_PORT = 6379;

    protected RedisClient redisClient;

    Router router= Router.router(vertx);

    private void initRedis() {
        RedisOptions config = new RedisOptions();
        config.setHost(REDIS_HOST);
        config.setPort(REDIS_PORT);
        redisClient = RedisClient.create(vertx, config);
    }

    private void initRouting(){
        Set<String> allowHeaders = new HashSet<>();
        allowHeaders.add("x-requested-with");
        allowHeaders.add("Access-Control-Allow-Origin");
        allowHeaders.add("origin");
        allowHeaders.add("Content-Type");
        allowHeaders.add("accept");
        Set<HttpMethod> allowMethods = new HashSet<>();
        allowMethods.add(HttpMethod.GET);
        allowMethods.add(HttpMethod.POST);
        allowMethods.add(HttpMethod.DELETE);
        allowMethods.add(HttpMethod.PUT);

        router.route().handler(BodyHandler.create());
        router.route().handler(CorsHandler.create("*")
                .allowedHeaders(allowHeaders)
                .allowedMethods(allowMethods));

        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response
                    .putHeader("content-type", "text/html")
                    .end("<h1>Employee Transport Allocation System</h1>");
        });

    }
    protected void init(){
        initRedis();
        initRouting();
    }

    @Override
    public void start(Future<Void> future) {
        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(config().getInteger("http.port", HTTP_PORT),
                        config().getString("http.address", HTTP_HOST), result -> {
                            if (result.succeeded())
                                future.complete();
                            else
                                future.fail(result.cause());
                        });
    }

    /**
     * Wrap the result handler with failure handler (503 Service Unavailable)
     */

    protected  <T> Handler<AsyncResult<T>> resultHandler(RoutingContext context, Consumer<T> consumer) {
        return res -> {
            if (res.succeeded()) {
                consumer.accept(res.result());
            } else {
                serviceUnavailable(context);
            }
        };
    }
    protected void serviceUnavailable(RoutingContext context) {
        context.response().setStatusCode(503).end();
    }
    protected void sendError(int statusCode, HttpServerResponse response) {
        response.setStatusCode(statusCode).end();
    }
    protected void notFound(RoutingContext context) {
        context.response().setStatusCode(404).end();
    }
    protected void badRequest(RoutingContext context) {
        context.response().setStatusCode(400).end();
    }







}
