package com.yantranet.verticles;

import com.yantranet.Constants;
import com.yantranet.controllers.BookingController;
import com.yantranet.controllers.CabController;
import com.yantranet.controllers.EmployeeController;
import com.yantranet.controllers.RequestController;
import com.yantranet.services.IEmployeeService;
import io.vertx.core.*;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class MainVerticle extends BaseVerticle {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);


    private EmployeeController employeeController;
    private CabController cabController;
    private RequestController requestController;
    private BookingController bookingController;

    private void initControllers() {
        employeeController = new EmployeeController(vertx, redisClient);
        cabController = new CabController(vertx,redisClient);
        requestController = new RequestController(vertx,redisClient);
        bookingController = new BookingController(vertx,redisClient);
    }

    private void initHandlers() {
        router.get(Constants.EMPLOYEE_URL_CREATE_UPDATE_GETALL).handler(employeeController::getAll);
        router.post(Constants.EMPLOYEE_URL_CREATE_UPDATE_GETALL).handler(employeeController::create);
        router.get(Constants.EMPLOYEE_URL_GET_DELETE).handler(employeeController::get);
        router.put(Constants.EMPLOYEE_URL_CREATE_UPDATE_GETALL).handler(employeeController::update);
        router.delete(Constants.EMPLOYEE_URL_GET_DELETE).handler(employeeController::delete);

        router.get(Constants.CAB_URL_CREATE_UPDATE_GETALL).handler(cabController::getAll);
        router.post(Constants.CAB_URL_CREATE_UPDATE_GETALL).handler(cabController::create);
        router.get(Constants.CAB_URL_GET_DELETE).handler(cabController::get);
        router.put(Constants.CAB_URL_CREATE_UPDATE_GETALL).handler(cabController::update);
        router.delete(Constants.CAB_URL_GET_DELETE).handler(cabController::delete);
        router.put(Constants.CAB_URL_STATUSUPDATE).handler(cabController::updateStatus);

        router.post(Constants.REQUEST_URL_CREATE).handler(requestController::create);
        router.get(Constants.REQUEST_URL_GET).handler(requestController::get);

    }

    private void insertSource(){
        redisClient.hset(Constants.SOURCE,String.valueOf(Constants.MADHAPUR), Json.encode(Constants.MADHAPUR),null);
        redisClient.hset(Constants.SOURCE,String.valueOf(Constants.HIMAYATNAGAR),Json.encode(Constants.HIMAYATNAGAR),null);
        redisClient.hset(Constants.SOURCE,String.valueOf(Constants.MGBS),Json.encode(Constants.MGBS),null);
        redisClient.hset(Constants.SOURCE,String.valueOf(Constants.MIYAPUR),Json.encode(Constants.MIYAPUR),null);

    }

    @Override
    public void start(Future<Void> future) {
        LOGGER.info("starting main verticle");
        super.init();
        initControllers();
        initHandlers();
        insertSource();
        super.start(future);
    }
}