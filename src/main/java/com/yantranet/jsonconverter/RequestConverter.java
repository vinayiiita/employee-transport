package com.yantranet.jsonconverter;

import com.yantranet.entities.Request;
import io.vertx.core.json.JsonObject;

public class RequestConverter {

    public static void fromJson(JsonObject json, Request obj) {

        if (json.getValue("id") instanceof String) {
            obj.setId((json.getValue("id")).toString());
        }

        if (json.getValue("employeeId") instanceof String) {
            obj.setEmployeeId((json.getValue("employeeId")).toString());
        }

        if (json.getValue("sourceLocation") instanceof String) {
            obj.setSourceLocation((json.getValue("sourceLocation")).toString());
        }

        if (json.getValue("dateTimeOfJourney") instanceof String) {
            obj.setDateTimeOfJourney((json.getValue("dateTimeOfJourney")).toString());
        }
        if (json.getValue("status") instanceof String) {
            String status = (json.getValue("status")).toString();
            switch (status) {
                case "GENERATED":
                    obj.setStatus(Request.REQUEST_STATUS.GENERATED);
                    break;
                case "PROCESSED":
                    obj.setStatus(Request.REQUEST_STATUS.PROCESSED);
                    break;
                case "FAILED":
                    obj.setStatus(Request.REQUEST_STATUS.FAILED);
                    break;
                case "CLOSED":
                    obj.setStatus(Request.REQUEST_STATUS.CLOSED);
                    break;
                case "CANCELLED":
                    obj.setStatus(Request.REQUEST_STATUS.CANCELLED);
                    break;
                default:
                    obj.setStatus(Request.REQUEST_STATUS.FAILED);
            }
        }
        if (json.getValue("comments") instanceof String) {
            obj.setComments((json.getValue("comments")).toString());
        }
        if (json.getValue("bookingId") instanceof String) {
            obj.setBookingId((json.getValue("bookingId")).toString());
        }

        if (json.getValue("requestCreationDate") instanceof String) {
            obj.setRequestCreationDate((json.getValue("requestCreationDate")).toString());
        }
    }

    public static void toJson(Request obj, JsonObject json) {
        json.put("id", obj.getId());
        if (obj.getEmployeeId() != null) {
            json.put("employeeId", obj.getEmployeeId());
        }
        if (obj.getEmployeeId() != null) {
            json.put("sourceLocation", obj.getSourceLocation());
        }
        if (obj.getEmployeeId() != null) {
            json.put("dateTimeOfJourney", obj.getDateTimeOfJourney());
        }
        if (obj.getEmployeeId() != null) {
            json.put("status", obj.getStatus());
        }
        if (obj.getEmployeeId() != null) {
            json.put("comments", obj.getComments());
        }
        if (obj.getEmployeeId() != null) {
            json.put("bookingId", obj.getBookingId());
        }
        if (obj.getEmployeeId() != null) {
            json.put("requestCreationDate", obj.getRequestCreationDate());
        }

    }
}
