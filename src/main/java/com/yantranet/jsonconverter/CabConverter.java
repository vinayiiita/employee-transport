package com.yantranet.jsonconverter;

import com.yantranet.entities.Cab;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class CabConverter {
static Logger Logger = LoggerFactory.getLogger(CabConverter.class);
    public static void fromJson(JsonObject json, Cab obj) {

       // Logger.info(json);

        if (json.getValue("id") instanceof String) {
            obj.setId((json.getValue("id")).toString());
        }
        if (json.getValue("registrationNumber") instanceof String) {
            obj.setRegistrationNumber((json.getValue("registrationNumber")).toString());
        }
        if (json.getValue("driverId") instanceof String) {
            obj.setDriverId((json.getValue("driverId")).toString());
        }
        if (json.getValue("status") instanceof String) {
            String status = (json.getValue("status")).toString();
            if (Cab.CAB_STATUS.AVAILABLE.toString().equalsIgnoreCase(status))
                obj.setStatus(Cab.CAB_STATUS.AVAILABLE);
            else
                obj.setStatus(Cab.CAB_STATUS.UNAVAILABLE);
        }
        if (json.getValue("comments") instanceof String) {
            obj.setComments((json.getValue("comments")).toString());
        }
        if (json.getValue("vacancy") instanceof Integer) {
            obj.setVacancy(Integer.parseInt((json.getValue("vacancy")).toString()));
        }
        //Logger.info(obj.toString());

    }

    public static void toJson(Cab obj, JsonObject json) {
        json.put("id", obj.getId());
        if (obj.getRegistrationNumber() != null) {
            json.put("registrationNumber", obj.getRegistrationNumber());
        }
        if (obj.getDriverId() != null) {
            json.put("driverId", obj.getDriverId());
        }
        if (obj.getStatus() != null) {
            json.put("status", obj.getStatus());
        }
        if (obj.getComments() != null) {
            json.put("co", obj.getComments());
        }
        if (obj.getVacancy() != null) {
            json.put("vacancy", obj.getVacancy());
        }
    }

}
