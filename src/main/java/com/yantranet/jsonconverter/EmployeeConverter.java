package com.yantranet.jsonconverter;

import com.yantranet.entities.Employee;
import io.vertx.core.json.JsonObject;

public class EmployeeConverter {


    public static void fromJson(JsonObject json, Employee obj) {

        if (json.getValue("id") instanceof String) {
            obj.setId((json.getValue("id")).toString());
        }

        if (json.getValue("name") instanceof String) {
            obj.setName((json.getValue("name")).toString());
        }
        if (json.getValue("designation") instanceof String) {
            obj.setDesignation((json.getValue("designation")).toString());
        }
        if (json.getValue("joiningDate") instanceof String) {
            obj.setJoiningDate((json.getValue("joiningDate")).toString());
        }
        if (json.getValue("email") instanceof String) {
            obj.setEmail((json.getValue("email")).toString());
        }
        if (json.getValue("phone") instanceof String) {
            obj.setPhone((json.getValue("phone")).toString());
        }
        if (json.getValue("address") instanceof String) {
            obj.setAddress((json.getValue("address")).toString());
        }
    }

    public static void toJson(Employee obj, JsonObject json) {

        json.put("id", obj.getId());
        if (obj.getName() != null) {
            json.put("name", obj.getName());
        }
        if (obj.getDesignation() != null) {
            json.put("designation", obj.getDesignation());
        }

        if (obj.getJoiningDate() != null) {
            json.put("joiningDate", obj.getJoiningDate());
        }
        if (obj.getEmail() != null) {
            json.put("email", obj.getEmail());
        }
        if (obj.getPhone() != null) {
            json.put("phone", obj.getPhone());
        }
        if (obj.getAddress() != null) {
            json.put("address", obj.getAddress());
        }

    }
}
