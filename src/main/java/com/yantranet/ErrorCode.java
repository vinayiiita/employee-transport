package com.yantranet;

public class ErrorCode {
    public final static String CAB_NOT_AVAILABLE = "CAB_NOT_AVAILABLE";
    public final static String SOURCE_INVALID = "SOURCE_INVALID";
    public final static String INVALID_TRIP_TIME = "INVALID_TRIP_TIME";
    public final static String REQUEST_NOT_POSSIBLE = "REQUEST_NOT_POSSIBLE";
    public final static String CANCELLATION_NOT_POSSIBLE = "CANCELLATION_NOT_POSSIBLE";
    public final static String OTHER ="OTHER";

}
