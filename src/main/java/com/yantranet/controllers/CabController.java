package com.yantranet.controllers;

import com.yantranet.Constants;
import com.yantranet.entities.Cab;
import com.yantranet.entities.Employee;
import com.yantranet.services.CabService;
import com.yantranet.services.ICabService;
import com.yantranet.services.IEmployeeService;
import io.vertx.core.Vertx;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import io.vertx.redis.RedisClient;
import sun.rmi.runtime.Log;

public class CabController extends AbstractController{

    public CabController(Vertx vertx, RedisClient redis) {
        //super(vertx, redis);
        service = new CabService(vertx,redis);
    }
    private static final Logger LOGGER = LoggerFactory.getLogger(CabController.class);
    private ICabService service;

    public void get(RoutingContext routingContext){
        LOGGER.info("get cab");
        String id = routingContext.request().getParam("id");
        if (id == null) {
            sendError(400, routingContext.response());
            return;
        }
        service.getCab(id).setHandler(resultHandler(routingContext, res -> {
            if (!res.isPresent())
                notFound(routingContext);
            else {
                final String encoded = Json.encodePrettily(res.get());
                routingContext.response()
                        .putHeader("content-type", "application/json")
                        .end(encoded);
            }
        }));
    }
    public void getAll(RoutingContext routingContext){
        LOGGER.info("get all cabs");
        service.getAll().setHandler(resultHandler(routingContext, res -> {
            if (res == null) {
                routingContext.response()
                        .putHeader("content-type", "application/json")
                        .end("[]");
            } else {
                final String encoded = Json.encodePrettily(res);
                routingContext.response()
                        .putHeader("content-type", "application/json")
                        .end(encoded);
            }
        }));

    }

    public void create(RoutingContext routingContext){
        LOGGER.info("create cab");
        try {
            final Cab cab = wrapObject(new Cab(routingContext.getBodyAsString(),true), routingContext);
            final String encoded = Json.encodePrettily(cab);

            service.create(cab).setHandler(resultHandler(routingContext, res -> {
                if (res) {
                    routingContext.response()
                            .setStatusCode(201)
                            .putHeader("content-type", "application/json")
                            .end(encoded);
                } else {
                    serviceUnavailable(routingContext);
                }
            }));
        } catch (DecodeException e) {
            sendError(400, routingContext.response());
        }

    }

    public void update(RoutingContext routingContext){
        LOGGER.info("update cab");

        try {
            String id = routingContext.request().getParam("id");
            final Cab cab = new Cab(routingContext.getBodyAsString(),false);
            // handle error
            if (cab == null) {
                sendError(400, routingContext.response());
                return;
            }
            service.update(cab)
                    .setHandler(resultHandler(routingContext, res -> {
                        if (res == null)
                            notFound(routingContext);
                        else {
                            final String encoded = Json.encodePrettily(res);
                            routingContext.response()
                                    .putHeader("content-type", "application/json")
                                    .end(encoded);
                        }
                    }));
        } catch (DecodeException e) {
            badRequest(routingContext);
        }

    }

    public void updateStatus(RoutingContext routingContext){

        LOGGER.info("update cab status");

        try {
            String id = routingContext.request().getParam("id");
            String status = routingContext.request().getParam("status");
            Cab.CAB_STATUS st = status.equalsIgnoreCase(Cab.CAB_STATUS.AVAILABLE.toString())? Cab.CAB_STATUS.AVAILABLE: Cab.CAB_STATUS.UNAVAILABLE;
            //LOGGER.info(st.toString());
            //LOGGER.info(id);
            service.updateStatus(id,st).setHandler(resultHandler(routingContext, res -> {
                        if (res == null)
                            notFound(routingContext);
                        else {
                            final String encoded = Json.encodePrettily(res);
                            routingContext.response()
                                    .putHeader("content-type", "application/json")
                                    .end(encoded);
                        }
                    }));
        } catch (DecodeException e) {
            badRequest(routingContext);
        }

    }
    public void delete(RoutingContext routingContext){
        LOGGER.info("delete cab");
        String id = routingContext.request().getParam("id");
        LOGGER.info(id);
        service.delete(id)
                .setHandler(deleteResultHandler(routingContext));

    }
    private Cab wrapObject(Cab cab, RoutingContext context) {
        String id = cab.getId();
        return cab;
    }


}
