package com.yantranet.controllers;

import com.yantranet.entities.Employee;
import com.yantranet.entities.Request;
import com.yantranet.services.IRequest;
import com.yantranet.services.RequestService;
import io.vertx.core.Vertx;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import io.vertx.redis.RedisClient;
import com.yantranet.ErrorCode;

public class RequestController extends AbstractController {

    public RequestController(Vertx vertx, RedisClient redis) {
        service = new RequestService(vertx,redis);
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestController.class);
    private IRequest service;

    public void get(RoutingContext routingContext){
        LOGGER.info("get request");
        String id = routingContext.request().getParam("id");
        if (id == null) {
            sendError(400, routingContext.response());
            return;
        }
        service.getRequest(id).setHandler(resultHandler(routingContext, res -> {
            if (!res.isPresent())
                notFound(routingContext);
            else {
                final String encoded = Json.encodePrettily(res.get());
                routingContext.response()
                        .putHeader("content-type", "application/json")
                        .end(encoded);
            }
        }));
    }
    public void create(RoutingContext routingContext){
        LOGGER.info("create Request");
        try {
            final Request employee = wrapObject(new Request(routingContext.getBodyAsString(),true), routingContext);
            final String encoded = Json.encodePrettily(employee);
            service.create(employee).setHandler(resultHandler(routingContext, res -> {
                if (res!=null) {

                    switch (res) {
                        case ErrorCode.CAB_NOT_AVAILABLE :
                    }

                    LOGGER.info(res);
                    routingContext.response()
                            .setStatusCode(201)
                            .putHeader("content-type", "application/json")
                            .end(Json.encode("requestId: "+res));

                } else {
                    //serviceUnavailable(routingContext);
                    routingContext.response().setStatusCode(503).end();
                }
            }));
        } catch (DecodeException e) {
            sendError(400, routingContext.response());
        }
    }

    private Request wrapObject(Request request, RoutingContext context) {
        String id = request.getId();
        return request;
    }

}
