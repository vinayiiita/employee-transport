/*
package com.yantranet.controllers;

import com.yantranet.entities.Request;
import com.yantranet.entities.Source;
import com.yantranet.services.BookingService;
import com.yantranet.services.IBooking;
import com.yantranet.services.SourceService;
import io.vertx.core.Vertx;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import io.vertx.redis.RedisClient;

public class SourceController extends AbstractController {


    public SourceController(Vertx vertx, RedisClient redis) {
        service = new SourceService(vertx,redis);
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(SourceController.class);
    private SourceService service;

    public void create(RoutingContext routingContext){
        LOGGER.info("create source");
        try {
            final Source source = wrapObject(new Source(routingContext.getBodyAsString(),true), routingContext);
            final String encoded = Json.encodePrettily(source);
            service.create(source).setHandler(resultHandler(routingContext, res -> {
                if (res!=null) {

                    LOGGER.info(res);
                    routingContext.response()
                            .setStatusCode(201)
                            .putHeader("content-type", "application/json")
                            .end(Json.encode("requestId: "+res));

                } else {
                    //serviceUnavailable(routingContext);
                    routingContext.response().setStatusCode(503).end();
                }
            }));
        } catch (DecodeException e) {
            sendError(400, routingContext.response());
        }
    }


    private Request wrapObject(Request request, RoutingContext context) {
        String id = request.getId();
        return request;
    }

}
*/
