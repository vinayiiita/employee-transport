package com.yantranet.controllers;

import com.yantranet.services.BookingService;
import com.yantranet.services.IBooking;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.redis.RedisClient;

public class BookingController extends AbstractController {

    public BookingController(Vertx vertx, RedisClient redis) {
        service = new BookingService(vertx,redis);
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);
    private IBooking service;

}
