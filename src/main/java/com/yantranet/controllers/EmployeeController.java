package com.yantranet.controllers;

import com.yantranet.Constants;
import com.yantranet.entities.Employee;
import com.yantranet.services.EmployeeService;
import com.yantranet.services.IEmployeeService;
import io.vertx.core.Vertx;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import io.vertx.redis.RedisClient;

public class EmployeeController extends AbstractController {

    public EmployeeController(Vertx vertx, RedisClient redis) {
        service = new EmployeeService(vertx,redis);
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);
    private IEmployeeService service;

    public void get(RoutingContext routingContext){
        LOGGER.info("get employee");
        String id = routingContext.request().getParam("id");
        if (id == null) {
            sendError(400, routingContext.response());
            return;
        }

        service.getEmployee(id).setHandler(resultHandler(routingContext, res -> {
            if (!res.isPresent())
                notFound(routingContext);
            else {
                final String encoded = Json.encodePrettily(res.get());
                routingContext.response()
                        .putHeader("content-type", "application/json")
                        .end(encoded);
            }
        }));

    }
    public void getAll(RoutingContext routingContext){
        LOGGER.info("get all employees");
        service.getAll().setHandler(resultHandler(routingContext, res -> {
            if (res == null) {
                routingContext.response()
                        .putHeader("content-type", "application/json")
                        .end("[]");
            } else {
                final String encoded = Json.encodePrettily(res);
                routingContext.response()
                        .putHeader("content-type", "application/json")
                        .end(encoded);
            }
        }));

    }

    public void create(RoutingContext routingContext){
        LOGGER.info("create employee");
        try {
            final Employee employee = wrapObject(new Employee(routingContext.getBodyAsString(),true), routingContext);
            final String encoded = Json.encodePrettily(employee);

            service.create(employee).setHandler(resultHandler(routingContext, res -> {
                if (res !=null) {
                    routingContext.response()
                            .setStatusCode(201)
                            .putHeader("content-type", "application/json")
                            .end(encoded);
                } else {
                    serviceUnavailable(routingContext);
                }
            }));
        } catch (DecodeException e) {
            sendError(400, routingContext.response());
        }
    }

    public void update(RoutingContext routingContext){
        LOGGER.info("update employee");

        try {
            String id = routingContext.request().getParam("id");
            final Employee employee = new Employee(routingContext.getBodyAsString(),false);
            // handle error
            if (employee == null) {
                sendError(400, routingContext.response());
                return;
            }
            service.update(employee)
                    .setHandler(resultHandler(routingContext, res -> {
                        if (res == null)
                            notFound(routingContext);
                        else {
                            final String encoded = Json.encodePrettily(res);
                            routingContext.response()
                                    .putHeader("content-type", "application/json")
                                    .end(encoded);
                        }
                    }));
        } catch (DecodeException e) {
            badRequest(routingContext);
        }

    }
    public void delete(RoutingContext routingContext){
        LOGGER.info("delete employee");
        String id = routingContext.request().getParam("id");
        LOGGER.info(id);
        service.delete(id)
                .setHandler(deleteResultHandler(routingContext));

    }
    private Employee wrapObject(Employee employee, RoutingContext context) {
        String id = employee.getId();
        return employee;
    }



}
