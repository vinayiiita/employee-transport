package com.yantranet.entities;

import com.yantranet.jsonconverter.RequestConverter;
import io.vertx.core.json.JsonObject;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class Request {


    private static final AtomicInteger counter = new AtomicInteger(1);
    private String id;
    private String employeeId;
    private String sourceLocation;
    private String dateTimeOfJourney;

    private REQUEST_STATUS status;
    private String comments;
    private String bookingId;
    private String requestCreationDate;

    public enum REQUEST_STATUS{
        GENERATED,PROCESSED,FAILED, CLOSED, CANCELLED
    }

    public Request(String id, String employeeId, String sourceLocation, String dateTimeOfJourney, REQUEST_STATUS status, String comments, String bookingId, String requestCreationDate) {
        this.id = id;
        this.employeeId = employeeId;
        this.sourceLocation = sourceLocation;
        this.dateTimeOfJourney = dateTimeOfJourney;
        this.status = status;
        this.comments = comments;
        this.bookingId = bookingId;
        this.requestCreationDate = requestCreationDate;
    }

    public Request(String employeeId, String sourceLocation, String dateTimeOfJourney) {
        this.employeeId = employeeId;
        this.sourceLocation = sourceLocation;
        this.dateTimeOfJourney = dateTimeOfJourney;
    }

    public Request(String jsonStr, boolean isCreate) {
        RequestConverter.fromJson(new JsonObject(jsonStr), this);
        if (isCreate)
            this.setId(UUID.randomUUID().toString());
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getSourceLocation() {
        return sourceLocation;
    }

    public void setSourceLocation(String sourceLocation) {
        this.sourceLocation = sourceLocation;
    }

    public String getDateTimeOfJourney() {
        return dateTimeOfJourney;
    }

    public void setDateTimeOfJourney(String dateTimeOfJourney) {
        this.dateTimeOfJourney = dateTimeOfJourney;
    }

    public REQUEST_STATUS getStatus() {
        return status;
    }

    public void setStatus(REQUEST_STATUS status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getRequestCreationDate() {
        return requestCreationDate;
    }

    public void setRequestCreationDate(String requestCreationDate) {
        this.requestCreationDate = requestCreationDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Request request = (Request) o;

        if (id != request.id) return false;
        return employeeId == request.employeeId;
    }

    @Override
    public int hashCode() {
       return id.hashCode();
    }
}
