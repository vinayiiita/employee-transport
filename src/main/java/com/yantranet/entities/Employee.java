package com.yantranet.entities;

import com.yantranet.jsonconverter.EmployeeConverter;
import io.vertx.core.json.JsonObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class Employee {

    private static final AtomicInteger counter = new AtomicInteger(1);
    private String id;
    private String name;
    private String designation;
    private String joiningDate;
    private String email;
    private String phone;
    private String address;

    public Employee(String id, String name, String designation, String joiningDate, String email, String phone, String address) {
        this.id = id;
        this.name = name;
        this.designation = designation;
        this.joiningDate = joiningDate;
        this.email = email;
        this.phone = phone;
        this.address = address;
    }

    public Employee(String jsonStr, boolean isCreate) {
        EmployeeConverter.fromJson(new JsonObject(jsonStr), this);
        if (isCreate)
            this.setId(UUID.randomUUID().toString());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public static int getIncId() {
        return counter.get();
    }

    public static void setIncIdWith(int n) {
        counter.set(n);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (id != employee.id) return false;
        return name.equals(employee.name);
    }

    @Override
    public int hashCode() {
        String result = id;
        return id.hashCode();
    }

    private <T> T getOrElse(T value, T defaultValue) {
        return value == null ? defaultValue : value;
    }
    public Employee merge(Employee todo) {
        return new Employee(id,
                getOrElse(todo.name, name),
                getOrElse(todo.designation, designation),
                getOrElse(todo.joiningDate, joiningDate),
                getOrElse(todo.email, email),
                getOrElse(todo.phone, phone),
                getOrElse(todo.address, address));
    }
}
