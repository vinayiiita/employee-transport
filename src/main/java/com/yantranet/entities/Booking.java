package com.yantranet.entities;

import java.util.concurrent.atomic.AtomicInteger;

public class Booking {

    private static final AtomicInteger counter = new AtomicInteger(1);

    private int id;
    private String sourceLocation;
    private String dateTimeOfJourney;
    private BOOKING_STATUS status;
    private String passengerDetails;
    private String vehicleDetails;
    private String driverDetails;

    public Booking(int id, String sourceLocation, String dateTimeOfJourney, BOOKING_STATUS status, String passengerDetails, String vehicleDetails, String driverDetails) {
        this.id = id;
        this.sourceLocation = sourceLocation;
        this.dateTimeOfJourney = dateTimeOfJourney;
        this.status = status;
        this.passengerDetails = passengerDetails;
        this.vehicleDetails = vehicleDetails;
        this.driverDetails = driverDetails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSourceLocation() {
        return sourceLocation;
    }

    public void setSourceLocation(String sourceLocation) {
        this.sourceLocation = sourceLocation;
    }

    public String getDateTimeOfJourney() {
        return dateTimeOfJourney;
    }

    public void setDateTimeOfJourney(String dateTimeOfJourney) {
        this.dateTimeOfJourney = dateTimeOfJourney;
    }

    public BOOKING_STATUS getStatus() {
        return status;
    }

    public void setStatus(BOOKING_STATUS status) {
        this.status = status;
    }

    public String getPassengerDetails() {
        return passengerDetails;
    }

    public void setPassengerDetails(String passengerDetails) {
        this.passengerDetails = passengerDetails;
    }

    public String getVehicleDetails() {
        return vehicleDetails;
    }

    public void setVehicleDetails(String vehicleDetails) {
        this.vehicleDetails = vehicleDetails;
    }

    public String getDriverDetails() {
        return driverDetails;
    }

    public void setDriverDetails(String driverDetails) {
        this.driverDetails = driverDetails;
    }

    public void setIncId() {
        this.id = counter.incrementAndGet();
    }

    public static int getIncId() {
        return counter.get();
    }

    public static void setIncIdWith(int n) {
        counter.set(n);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Booking booking = (Booking) o;

        if (id != booking.id) return false;
        return sourceLocation != null ? sourceLocation.equals(booking.sourceLocation) : booking.sourceLocation == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (sourceLocation != null ? sourceLocation.hashCode() : 0);
        return result;
    }

    private enum BOOKING_STATUS{
        CONFIRMED, CANCELLED
    }
}
