package com.yantranet.entities;

import com.yantranet.jsonconverter.CabConverter;
import com.yantranet.jsonconverter.EmployeeConverter;
import io.vertx.core.json.JsonObject;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class Cab {

    private static final AtomicInteger counter = new AtomicInteger(1);

    private String id;
    private String registrationNumber;
    private String driverId;
    private CAB_STATUS status;
    private String comments;
    private Integer vacancy;

    public Cab(){

    }

    public Cab(String id, String registrationNumber, String driverId, CAB_STATUS status, String comments, int vacancy) {
        this.id = id;
        this.registrationNumber = registrationNumber;
        this.driverId = driverId;
        this.status = status;
        this.comments = comments;
        this.vacancy = vacancy;
    }


    public Cab(String jsonStr, boolean isCreate) {
        CabConverter.fromJson(new JsonObject(jsonStr), this);
        if (isCreate)
            this.setId(UUID.randomUUID().toString());
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public CAB_STATUS getStatus() {
        return status;
    }

    public void setStatus(CAB_STATUS status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getVacancy() {
        return vacancy;
    }

    public void setVacancy(int vacancy) {
        this.vacancy = vacancy;
    }

    public enum CAB_STATUS{
        AVAILABLE,UNAVAILABLE
    }

    @Override
    public String toString() {
        return "[ id : " + this.getId() + ", regNo : " + registrationNumber + ", driverId: " + driverId + ", status: " + status.toString() + ", comments: " + comments + ", vacancy" + vacancy + "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cab cab = (Cab) o;
        if (id != cab.id) return false;
        return registrationNumber.equals(cab.registrationNumber);
    }

    @Override
    public int hashCode() {
        if(id!=null)
        return id.hashCode();
        return 1;
    }


    private <T> T getOrElse(T value, T defaultValue) {
        return value == null ? defaultValue : value;
    }
    public Cab merge(Cab todo) {
        return new Cab(id,
                getOrElse(todo.registrationNumber, registrationNumber),
                getOrElse(todo.driverId, driverId),
                getOrElse(todo.status, status),
                getOrElse(todo.comments, comments),
                getOrElse(todo.vacancy, vacancy));
    }
}
