package com.yantranet;

public class Constants {

    public static final String EMPLOYEE_KEY = "employee";
    public static final String BOOKING_KEY = "booking";
    public static final String CAB_KEY = "cab";
    public static final String REQUEST_KEY = "request";

    public static final String EMPLOYEE_URL_CREATE_UPDATE_GETALL ="/employees";
    public static final String EMPLOYEE_URL_GET_DELETE = "/employees/:id";
    //public static final String EMPLOYEE_URL_GET_ALL="/employees";

    public static final String CAB_URL_CREATE_UPDATE_GETALL="/cabs";
    public static final String CAB_URL_GET_DELETE="/cabs/:id";
    public static final String CAB_URL_STATUSUPDATE="/cabs/:id/:status";

    public static final String REQUEST_URL_CREATE="/request";
    public static final String REQUEST_URL_GET="/request/:id";

    public static final String BOOKING_URL_GET="/booking/:id";


    public static final String SOURCE = "source";
    public static final String MADHAPUR = "MADHAPUR";
    public static final String MIYAPUR = "MIYAPUR";
    public static final String MGBS = "MGBS";
    public static final String HIMAYATNAGAR = "HIMAYATNAGAR";


}
